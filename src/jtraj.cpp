#include "mbzirc_polytraj/jtraj.h"
#include <iostream>
#include <fstream>

namespace CA
{
void jTraj::computeTrajectory(const nav_msgs::Odometry q0, const nav_msgs::Odometry q1, const double t, const int n)
{
  m_timeScale = t;
  if (n % 2 == 0)
      m_steps = n;
  else
      m_steps = n+1;
 
  CA::Vector3D startXYZ = CA::msgc(q0.pose.pose.position);
  CA::Vector3D goalXYZ = CA::msgc(q1.pose.pose.position);
   
  CA::Vector3D startXYZd = CA::msgc(q0.twist.twist.linear);
  CA::Vector3D goalXYZd = CA::msgc(q1.twist.twist.linear);

  CA::QuatD orientation = CA::msgc(q0.pose.pose.orientation);
  CA::Vector3D attitude = CA::quatToEuler(orientation);
  double startHeading = attitude[2];
  double startHeadingd = q0.twist.twist.angular.z; 

  orientation = CA::msgc(q1.pose.pose.orientation);
  attitude = CA::quatToEuler(orientation);
  double goalHeading = attitude[2]; 
  double goalHeadingd = q1.twist.twist.angular.z; 
    
  //Compute the parameters for three polynomials (x, y, z)
  for(int i = 0; i < 3; i++)
  {
    /*
    % compute the polynomial coefficients
    A = 6*(q1 - q0) - 3*(qd1+qd0)*tscal;
    B = -15*(q1 - q0) + (8*qd0 + 7*qd1)*tscal;
    C = 10*(q1 - q0) - (6*qd0 + 4*qd1)*tscal;
    E = qd0*tscal; % as the t vector has been normalized
    F = q0;
    */ 
   
    A[i] = 6*(goalXYZ[i] - startXYZ[i]) - 3*(goalXYZd[i] + startXYZd[i])*m_timeScale;
    B[i] = -15*(goalXYZ[i] - startXYZ[i]) + (8*startXYZd[i] + 7*goalXYZd[i])*m_timeScale;
    C[i] = 10*(goalXYZ[i] - startXYZ[i]) - (6*startXYZd[i] + 4*goalXYZd[i])*m_timeScale;
    E[i] = startXYZd[i]*m_timeScale;
    F[i] = startXYZ[i];
  }

  //Compute the parameters for the polynomial for heading
  A[3] = 6*(goalHeading - startHeading) - 3*(goalHeadingd + startHeadingd)*m_timeScale;
  B[3] = -15*(goalHeading - startHeading) + (8*startHeadingd + 7*goalHeadingd)*m_timeScale;
  C[3] = 10*(goalHeading - startHeading) - (6*startHeadingd + 4*goalHeadingd)*m_timeScale;
  E[3] = startHeadingd*m_timeScale;
  F[3] = startHeading;  

  // Compute the trajectories
  std::vector<CA::State, Eigen::aligned_allocator<CA::Vector3D> > state_list;
  CA::State state;

  for (int i=0; i<4; i++){
  	largest_speed[i]=0.0;
	largest_acceleration[i]=0.0;
  }
  largest_linear_acceleration=0.0;
  largest_linear_speed=0.0;
  largest_pitch_angle=0.0;
  
  double tstep=(double)(1.0/m_steps);
 
  for (double t = 0.0; t<=1.0; t=t+tstep){
		
  	double t5, t4, t3, t2;
    	t5 = pow(t,5);
  	t4 = pow(t,4);
  	t3 = pow(t,3);
  	t2 = pow(t,2);
        CA::Vector3D q, qd, qdd;
	double heading, headingd, headingdd;  

	for(int i = 0; i < 3; i++)
  	{
    		q[i] = A[i]*t5 + B[i]*t4 + C[i]*t3 + E[i]*t + F[i];     
    		qd[i] = (5*A[i]*t4 + 4*B[i]*t3 + 3*C[i]*t2 + E[i])/m_timeScale;     
    		qdd[i] = ((20*A[i]*t3 + 12*B[i]*t2 + 6*C[i]*t)/m_timeScale)/m_timeScale; 
                
                if (fabs(qd[i])>largest_speed[i])
			largest_speed[i]=fabs(qd[i]);
		if (fabs(qdd[i])>largest_acceleration[i])
			largest_acceleration[i]=fabs(qdd[i]);
                
  	}
        // Store the maximum speed and acceleration
        if (qd.norm()>largest_linear_speed) 
		largest_linear_speed=qd.norm();
	if (qdd.norm()>largest_linear_acceleration) 
		largest_linear_acceleration=qdd.norm();
     
        heading = A[3]*t5 + B[3]*t4 + C[3]*t3 + E[3]*t + F[3];     
    	headingd = (5*A[3]*t4 + 4*B[3]*t3 + 3*C[3]*t2 + E[3])/m_timeScale;     
    	headingdd = ((20*A[3]*t3 + 12*B[3]*t2 + 6*C[3]*t)/m_timeScale)/m_timeScale;

	if (fabs(headingd)>largest_speed[3])
		largest_speed[3]=fabs(headingd);
	if (fabs(headingdd)>largest_acceleration[3])
		largest_acceleration[3]=fabs(headingdd);

	// Compute the pitch angle and store the maximum pitch angle - reference: Melinger and Kumar. ICRA 2011
        
        CA::Vector3D x, y, z=qdd; 
        z[2]=z[2]+9.8; // Assuming z pointing up
        z.normalize();
        x[0]=cos(heading);
	x[1]=sin(heading);
        x[2]=0.0;
        y=z.cross(x);
	y.normalize();
        x=y.cross(z);
        //tf::Matrix3x3 m(x[0], y[0], z[0], x[1], y[1], z[1], x[2], y[2], z[2]);
	double roll, pitch, yaw;
        //m.getEulerYPR(yaw, pitch, roll);
       // m.getRPY(roll, pitch, yaw);
        //ROS_INFO("1 Roll: %f Pitch: %f Yaw: %f CYaw: %f", roll, pitch, yaw, heading);
 
	// TODO - Check this! I borrow this from one of the MRSD student. This is not the same result given by getRPY but it makes more sense to me.
    	roll =  atan2(-z[1],z[2]);
    	pitch = atan2( z[0], cos(roll)*z[2] - sin(roll)*z[1]);
    	//yaw = atan2( -y[0], x[0]);
    	//ROS_INFO("2 Roll: %f Pitch: %f Yaw: %f CYaw: %f", roll, pitch, yaw, heading);
        if (fabs(pitch)>largest_pitch_angle)
		largest_pitch_angle=fabs(pitch);

	state.pose.position_m = q;  	
        state.rates.velocity_mps = qd;              
        state.pose.orientation_rad[2] = heading;
	state.rates.rot_rad[2] = headingd;
                
        state_list.push_back(state);    
  }
  m_trajectory.setLinear(state_list);

  /*std::ofstream myfile;
  myfile.open ("/home/gpereira/trajectory.txt");
  for (int i=0; i<state_list.size(); i++){
  	myfile << state_list[i].pose.position_m[0] << " " << state_list[i].pose.position_m[1] << " " << state_list[i].pose.position_m[2] << " " << state_list[i].pose.orientation_rad[2]<< " ";
	myfile << state_list[i].rates.velocity_mps[0] << " " << state_list[i].rates.velocity_mps[1] << " " << state_list[i].rates.velocity_mps[2] << " " << state_list[i].rates.rot_rad[2]<<"\n";
  }
  myfile.close();*/	       
}


bool jTraj::respectConstraints(double max_speed, double max_acceleration)
{
     if ((max_speed>=largest_linear_speed) && (max_acceleration>=largest_linear_acceleration))
		return true;
     else
		return false;
}
 
bool jTraj::respectConstraints(double max_speed, double max_yawRate, double max_pitchAngle)
{
     //ROS_INFO("In  %f %f %f", largest_linear_speed, largest_speed[3], largest_pitch_angle);
     //ROS_INFO("Out %f %f %f", max_speed, max_yawRate, max_pitchAngle);
     if ((max_speed>=largest_linear_speed)  && (max_yawRate>=largest_speed[3]) && (max_pitchAngle>=largest_pitch_angle))
		return true;
     else
		return false;
}

bool jTraj::respectConstraints(double max_linear_speed, double max_linear_acceleration, double max_angular_speed, double max_angular_acceleration)
{
     if ((max_linear_speed>=largest_linear_speed) && (max_linear_acceleration>=largest_linear_acceleration) && (max_angular_speed>=largest_speed[3]) && (max_angular_acceleration>=largest_acceleration[3]))
		return true;
     else
		return false;
}

CA::Trajectory jTraj::getTrajectory()
{
	return m_trajectory;
}

CA::Trajectory jTraj::computeStraightLineTrajectory(const nav_msgs::Odometry q0, const nav_msgs::Odometry q1, const int n){
  
  m_steps = n;
 
  CA::Vector3D startXYZ = CA::msgc(q0.pose.pose.position);
  CA::Vector3D goalXYZ = CA::msgc(q1.pose.pose.position);
   
  CA::Vector3D startXYZd = CA::msgc(q0.twist.twist.linear);
  CA::Vector3D goalXYZd = CA::msgc(q1.twist.twist.linear);

  CA::QuatD orientation = CA::msgc(q0.pose.pose.orientation);
  CA::Vector3D attitude = CA::quatToEuler(orientation);
  double startHeading = attitude[2];
  double startHeadingd = q0.twist.twist.angular.z; 

  orientation = CA::msgc(q1.pose.pose.orientation);
  attitude = CA::quatToEuler(orientation);
  double goalHeading = attitude[2]; 
  double goalHeadingd = q1.twist.twist.angular.z; 
  
  // Compute the trajectories
  std::vector<CA::State, Eigen::aligned_allocator<CA::Vector3D> > state_list;
  CA::State state;
  CA::Vector3D q, qd;
  double heading, headingd;

  for (double t = 0; t<m_steps-1; t++ ){
    
        for (int i=0; i<3; i++){
           q[i]=startXYZ[i]+t*(goalXYZ[i]-startXYZ[i])/(m_steps-1);	  
	}
	qd=startXYZd; // All points, except for the goal, have the same velocity
		
        heading = startHeading+t*(goalHeading-startHeading)/(m_steps-1);     
    	headingd = goalHeadingd;    // All points, except for the goal, have the same velocity
    	
	state.pose.position_m = q;  	
        state.rates.velocity_mps = qd;              
        state.pose.orientation_rad[2] = heading;
	state.rates.rot_rad[2] = headingd;
                
        state_list.push_back(state);    
  }
  
  // Last point -> speed is equal to the speed at the end
  q  = goalXYZ;
  qd = goalXYZd;
  heading  = goalHeading;      
  headingd = goalHeadingd;
  state.pose.position_m = q;  	
  state.rates.velocity_mps = qd;              
  state.pose.orientation_rad[2] = heading;
  state.rates.rot_rad[2] = headingd;
                
  state_list.push_back(state);   
  
  m_trajectory.setLinear(state_list);
  
  return m_trajectory;
  
}  
  
  
}

