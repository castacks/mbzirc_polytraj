#include <ca_common/math.h>
#include "tf/transform_datatypes.h"

namespace CA
{
class jTraj { // This clas implement the Peter Corke's matlab toolbox jtraj function for x, y, z and heading
   
   private:  
      double m_timeScale;
      int m_steps;
      double A[4], B[4], C[4], D[4], E[4], F[4];
      CA::Trajectory m_trajectory;
      double largest_speed[4], largest_acceleration[4];
      double largest_linear_speed, largest_linear_acceleration, largest_pitch_angle;	

  public:
     jTraj(){}
     
     void computeTrajectory(const nav_msgs::Odometry q0, const nav_msgs::Odometry q1, const double t, const int n);  
 
     bool respectConstraints(double max_speed, double max_acceleration); 

     bool respectConstraints(double max_speed, double max_yawRate, double max_pitchAngle);
     
     bool respectConstraints(double max_linear_speed, double max_linear_acceleration, double max_angular_speed, double max_angula_acceleration); 
     
     CA::Trajectory getTrajectory();
     
     CA::Trajectory computeStraightLineTrajectory(const nav_msgs::Odometry q0, const nav_msgs::Odometry q1, const int n); 

};
}

